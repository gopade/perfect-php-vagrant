#!/usr/bin/env bash
echo "Installing NewRelic..."

echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' | sudo tee /etc/apt/sources.list.d/newrelic.list
wget -O- https://download.newrelic.com/548C16BF.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install newrelic-php5

echo "Installing Phalcon 2..."

cd ~/build
wget -o /dev/null -O phalcon.zip https://github.com/phalcon/cphalcon/archive/master.zip
unzip phalcon.zip -d cphalcon > /dev/null
rm phalcon.zip
cd cphalcon/cphalcon-master
zephir install

#git clone -b 2.0.0 https://github.com/phalcon/cphalcon.git
#cd cphalcon
#zephir install

sudo echo extension=phalcon.so > /etc/php5/mods-available/phalcon.ini
sudo php5enmod phalcon
