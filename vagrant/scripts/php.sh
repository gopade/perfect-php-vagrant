#!/usr/bin/env bash

echo "Installing PHP..."

sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql postgresql-contrib
sudo apt-get -y install php5-cli php5-dev php5-fpm php5-curl php5-memcached memcached php5-intl php5-pgsql

sudo apt-get -y install newrelic-php5
sudo apt-get -y install memcached

sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

sudo gem intall compass
sudo npm install gulp-jquery
sudo npm install gulp-uglify

